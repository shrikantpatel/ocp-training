This is the Labs repository for the Practical OpenShift for Developers course. 

// deploy the project directly from git
oc new-app quay.io/practicalopenshift/hello-world --as-deployment-config
oc new-app https://gitlab.com/practical-openshift/hello-world.git --name demo-app-v1 --as-deployment-config -e MESSAGE=test1
oc new-app https://gitlab.com/practical-openshift/hello-world.git --name demo-app-v2 --as-deployment-config -e MESSAGE=test2
oc new-app shrikantpatel/myapp:latest --as-deployment-config

oc port-forward demo-app-v1-1-zwwhr 8080
oc port-forward demo-app-v2-1-mwpsn 8081

//watch 
oc get pods --watch

// deploy new version and rollback to old version
oc rollout latest dc/demo-app
oc rollback dc/demo-app

//expose the service via routes
oc expose svc demo-app-v1

// get below configuration from env
wget $HELLO_WORLD_POD_PORT_8080_TCP_ADDR:$HELLO_WORLD_POD_PORT_8080_TCP_PORT

//using config map
oc create configmap message-map --from-literal MESSAGE="Hello from configmap"
oc create configmap file-map --from-file=MESSAGE=message.txt
oc create configmap file-map --from-file=pods (path to directory)

//consume from config map inside of pod
oc set env dc/demo-app --from cm/message-map
oc set env dc/demo-app --from cm/file-map